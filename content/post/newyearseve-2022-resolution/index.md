+++
author = "unkwn1"
title = "Taking a Look Back @ 2022"
date = "2022-12-31"
description = "Take a quick look back at 2022. I will make sieze upon 2023; I love you Foxy."
tags = [
    "personal",
]
categories = [
    "Misc",
]
+++


# 2022 - When it Rains it Pours

2022 has been one hell of a year for me. I pretty much ceased to function after the passing of my dog Foxy in April - coupled  with the loss of my job and things have been incredibly trying. However, I seem to be and hope to remain on the path forward. 

---

![](https://cdn-user.veed.io/render/c086f9c8-7859-42bd-ba2a-3f649d1637f7.mp4)

![](https://unkwn1.dev/images/foxylady.jpg)

---

The most difficult part about losing Foxy was just how much she meant to me. Both what I knew and what I came to find out only afterwards. Foxy was my shadow - she went everywhere with me and loved just about everyone. Multiple neighbors and shop owners around my neighborhood cried with me when they found out. I would constantly find myself throughout the day staring at her expressive eyes thinking "I can't believe how lucky I am to have you" or remarking how wonderful she *just* is.

Foxy as I've come to understand was my unintentional support dog. She was there to calm me down in social situations and whenever life through me a curve ball I could always sit down, open my legs and know she would plop herself down on me - kind of like a weighted blanket.


Thankfully I'm not alone. I have Khaos with my, Foxy's brother. That's an introduction for another post maybe.

### From Marketing to Tech
I now find myself in a spot I'm reluctant to call a fresh start. I don't want to go back into Digital Marketing and haven't quite landed on what area of expertise to focus on moving forward. I know I want enter the traditional tech sector; but, whether  as an IT consultant, SysAdmin, Kubernetes Admin etc. is still up for debate.

Though the clearity of what sector I want to join I feel is sufficient to get started. Take for example a bit of my history:

> Before I got into digital marketing professionally I stated a highschooler with a random blog. From the innocence of a nerdy kid with an early internet blog it lead into a journey and fascination with SEO.
> That early self-taught skill was useful enough to help me find SEO contract jobs as an 17-18 year old via scouring Kijiji ads.
> Having been paid for work - in any amount - greatly increased my confidence in my skillset. I then started applying for positions in agencies.
> Upon being hired by a well known agency I was lucky enough to have not only a boss but a mentor who coached my on sales techniques whilst we drove to tradeshows. 
> Side note: (I would highly recommend [WSI | Comandix](https://www.simplifytheinternet.com/) to any GTA business looking for digital marketing sevices)  

### Tech - But What?

I really do enjoy systemadmin related stuff. Setting up clusters of computers is always exciting. However, traditional IT routes like sysadmins, devops engineers are sticklers for post secondary degress. One could argue the Digital Marketing industry does as well but, for some reason (probably ignorance) I believe the technicle fields will demand degress. 

Another way of looking at my concern would be to compared traditional IT to say pentesting. Pentesting being the new trendy field with less historical inclusion in education has lead to a more open job market.

This is likely just me finding excuses.... As I type the above I kind of see it. So this post, more than anything is my own gentle reminder whenver I look at my site to get away from 2022. Embrace 2023, sieze every moment and, most importantly remain positive!

### I love you Foxy. Until we meet again my love.

![](https://unkwn1.dev/images/thebestest_doggo.jpg)
