+++
author = "unkwn1"
title = "A Tool to Create Nginx Proxies SUPER EASILY"
date = "2022-06-08"
description = "A summary of the nginx proxy manager project and why I love it and you will too!"
toc = true
series = [
        "devops",
]
tags = [
    "docker",
    "networking",
]
categories = [
    "SysAdmin",
]
+++

I find proxies to be the heart of any server I'm setting up. Most services I deploy aren't exposed via the internet connected interface - usually they're exposed to localhost only. The need for a reverse proxy (cluster ingress) happens more often now that I'm using docker / kubernetes clusters.  Having to create a reverse proxy for every service is tedious. Enter the project [Nginx Proxy Manager](https://nginxproxymanager.com/).

Nginx proxy manager runs in a Docker container which has a management web UI accessible via port 81. A simple `docker-compose.yml` deployment could be something like the example below:

> if you're NOT using docker swarm replace the `deploy:` section with `restart: unless-stopped`

```yaml
version: '3.8'
services:
  nginx-proxies:
    image: 'jc21/nginx-proxy-manager:latest'
    deploy:
      restart_policy:
        condition: on-failure
    ports:
      - '81:81'
    volumes:
      - ./data:/data
      - ./letsencrypt:/etc/letsencrypt
```

The deployment contains two volumes for persistent storage. The latter entry containing the vitally important SSL certs. 

LetsEncrypt integration and easy of deployment being another huge benefit. I've even used nginx proxy manager to generate a certificate downloaded and then added to my portainer settings.

 
