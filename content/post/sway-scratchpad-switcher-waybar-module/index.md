---
title: "Creating a Custom Scratchpad Module for Waybar"
date: "2023-01-14"
draft: true
categories: ["Linux", "Waybar", "Scratchpad"]
---

In this project, I will be creating a custom module for the waybar, a highly customizable status bar for Linux. The module will display an active hidden scratchpad count on the waybar and when clicked, it will open a kitty terminal launching a shell script that uses the gum CLI (Command Line Interface) to allow the selection and focus of a scratchpad.

## Installation
First, I will need to install waybar and kitty, if not already installed. 

## Pyhon Tree Parsing Script
Next, I will create a shell script that uses the gum CLI to list and select the scratchpads. This script will take one argument, the name of the scratchpad to focus on. i

## Bash Script - `gum` TUI

## Waybar Custom Module Creation
Next, I will create a custom module for waybar that displays the active hidden scratchpad count. This module will be configured to execute `scratchpads.py` every 2 seconds. The `on-click` setting will be set to execute `tui.sh` in a kitty shell using `kitty sh`.  shell 
