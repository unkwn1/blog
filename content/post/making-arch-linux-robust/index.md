+++
author = "unkwn1"
title = "Making My Arch Daily Driver More Robust"
date = "2024-01-01"
description = "In this post, I’ll show you how I set up my system with `btrfs` and some useful tools that make it more robust and convenient"
tags = [
    "linux",
    "btrfs",
]
categories = [
    "operating systems",
    "arch linux",
]
series = [
    "Linux Stuff",
]
+++

If you use Arch or any other upstream/rolling operating system, you may encounter some mishaps from time to time - such as dependency issues, corruption, and so on. Ideally, this wouldn’t be a problem because we would always backup our data regularly… Right? Well, I admit that I’m not very diligent about backups when it comes to my personal system, even though I am very careful with production deployments for my work. That’s why I decided to switch to `btrfs` instead of `ext4` or `lvm` when I moved back to Arch. `btrfs` offers a great feature called snapshots, which lets you create and restore backups of your system easily and quickly. Snapshots can save you a lot of trouble if something goes wrong with your system.

In this post, I’ll show you how I set up my system with `btrfs` and some useful tools that make it more robust and convenient. For simplicity, I’ll assume that we’re only dealing with a root filesystem on an Arch installation, with the usual partition scheme separating `/`, `var`, `tmp`, etc. I’ll also assume that we’re using `grub` as the bootloader, not `systemd-boot`.

The first thing I did after installing Arch was to install the following tools: `btrfs-assistant` (GUIs are easymode), `timeshift`, `snapper`, `snap-pac` and, `grub-btrfs`. 

- `btrfs-assistant` is a graphical application for managing btrfs subvolumes and snapper snapshots. It allows you to create, delete, rename, mount, and unmount subvolumes, as well as to browse, restore, and compare snapshots. It also supports scheduling automatic snapshots and cleaning old snapshots based on retention policies. `btrfs-assistant` can help you to easily manage your btrfs file system and take advantage of its advanced features.
- `timeshift` is a system restore tool for Linux that uses btrfs snapshots to create restore points for your system. You can use it to backup and restore your system files and settings, and revert your system to a previous state in case of any problems. `timeshift` also supports scheduling and excluding files from snapshots. `timeshift` can help you to protect your system from accidental or malicious changes and recover from system failures.
- `snapper` is a command-line tool for creating and managing btrfs snapshots. It allows you to create, list, delete, and rollback snapshots, as well as to compare and diff snapshots. It also supports configuring snapshot policies and hooks for automatic snapshot creation and cleanup. `snapper` can help you to track and undo changes to your btrfs file system and perform data backup and recovery.
- `snap-pac` is a set of pacman hooks and scripts that automatically creates pre and post snapshots using snapper before and after pacman transactions. It also adds btrfs snapshots to the grub menu, allowing you to boot into snapshots. `snap-pac` can help you to easily revert your system to a previous state before or after a pacman operation, such as installing, updating, or removing packages.
- `grub-btrfs` is a grub script that adds btrfs snapshots to the grub menu, allowing you to boot into snapshots. It also supports detecting kernel, initramfs, and microcode files within snapshots, and generating grub entries accordingly. It also includes a systemd service that automatically updates grub entries when new snapshots are created or deleted. `grub-btrfs` can help you to easily boot into different snapshots and test or restore your system.

This setup makes it easy to fix most issues by rebooting your system and choosing a snapshot from the ‘advanced options’ menu in grub. You can then restore your system to a working state from the snapshot. I found this very useful on my thinkpad, where the latest mesa driver sometimes messed up my graphics. On a smaller scale, the snap-pac snapshots before and after package installation are a handy way to undo any dependency or version problems 
