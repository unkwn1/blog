+++
author = "unkwn1"
title = "Bash Script for Rsync Backups"
date = "2022-01-14"
description = "Growing tired of running the same commands manually each backup I wrote a little shell script to automate the process"
toc = true
tags = [
    "shell scripting",
	"rsync",
]
categories = [
    "Projects",
    "Coding"
]
+++

One thing Arch Linux teaches every n00b is the importance of routine backups. Growing tired of running the same commands manually each backup I wrote a little shell script to automate the process.

```bash
#! /bin/sh
#
# backup2desktop.sh
# Copyright (C) 2022 unkwn1 <unkwn1@x1root>
#
# Distributed under terms of the MIT license.
#

bkTar=`date +%F`-x1cbk
bkDest="/mnt/BACKUP/x1cbk-latest"

cat /home/unkwn1/.config/.banners/unkwn1.sh

echo "!! STARTING BACKUP !!"

rsync -azuq \
    --exclude={".cache/",".config/BraveSoftware/",".config/discord/",".config/chromium/",".config/tutanota-desktop/",".config/Code/Cache/"} \
    $HOME/* \
    debpc:$bkDest/

if [ "$?" -gt "0" ]; then
    echo "!! ERROR: RSYNC BACKUP FAILED !!"
    exit 1
fi

echo "!! BACKUP FINISHED !!"

echo "!! COMPRESSING BACKUP RESULT !!"

ssh debpc \
    tar czvf \
    /mnt/BACKUP/$bkTar-x1cbk.tar.gz \
    $bkDest/

if [ "$?" -gt "0" ]; then
    echo "!! ERROR: TAR COMPRESSION FAILED !!"
    exit 1

echo "!! BACKUP FINISHED !!"
```

[get the script](https://gitlab.com/unkwn1/dotfiles/-/snippets/2235324)

The script does a few things.

First it does an rsync archive backup from the source folder to a destination folder on my desktop. It includes most of my home folder with some overhead and clutter excluded (eg caches).

> During each step it will check the exit code of the last command to catch errors.

Second it will create a compressed Tar file from the contents of the destination folder.

Thats pretty much it. As long as the rsync and tar commands return 0 exit codes the script will finish and leave a small log file detailing the last run.

### Bonus

I've been toying with reviving an old project of mine [backpack backup](https://gitlab.com/unkwn1/backpack-backup). It's a python program that encrypts files / directories, compressed and backs up the files.

*NOTE:* I wouldn't recommend using it until my next release as I found a bug that doesn't allow the use of password protected keys.
