+++
author = "unkwn1"
title = "Forcing Firefox to use Wayland"
date = "2023-01-22"
description = "The quickest solution to this problem is setting environment variables `MOZ_ENABLE_WAYLAND=1`. **BUT** like most things I endavour the most common solution never seems to work. "
tags = [
	"firefox",
	"wayland",
	"sway",
]
categories = [
	"Quick Fixes",
]
+++

One of the many reasons I switched from i3 to sway was Wayland. Although Wayland is going to be the future it still tends to be a secondary option - even when apps have built in support. A prime example is Firefox which has for a couple years now had native Wayland support. However, like many apps if they detect x11 present on the machine it will default to xwayland usage instead of native Wayland. 

I guess that's probably best as a default to maintain broad stability; but, it's annoying and I want Firefox to run on Wayland natively *no xwayland :angry:*. The quickest solution to this problem is setting environment variables `MOZ_ENABLE_WAYLAND=1`. **BUT** like most things I endavour the most common solution never seems to work. I've added the variables to `.zshrc` as I normally do Firefox still ran in xwayland. So I added a script to `/etc/profiles.d/` exporting the variable to no avail.

> running firefox via terminal using `env MOZ_ENABLE_WAYLAND=1 firefox` worked perfectly

After a little bit of reddit lurking it stumbled upon a [wonderful post](https://www.reddit.com/r/archlinux/comments/ozubo9/using_firefox_on_wayland_make_sure_you_have_moz/) that helped me fix this and another issue where other apps weren't opening links in Firefox.

Seeing as how I launch Firefox almost always via `wofi` the best solution was to create a local (`~/.local/share/applications`) desktop file for Firefox changing the `Exec` lines to include the `env`. Below is an example of a `firefox.desktop` entry.

```conf
[Desktop Entry]
Version=1.0
Name=Firefox
GenericName=Web Browser
Comment=Browse the World Wide Web
Keywords=Internet;WWW;Browser;Web;Explorer
Exec=env MOZ_ENABLE_WAYLAND=1 /usr/lib/firefox/firefox %u
Icon=firefox
Terminal=false
X-MultipleArgs=false
Type=Application
MimeType=text/html;text/xml;application/xhtml+xml;x-scheme-handler/http;x-scheme-handler/https;application/x-xpinstall;application/pdf;application/json;
StartupNotify=true
StartupWMClass=firefox
Categories=Network;WebBrowser;
Actions=new-window
```

Upon creating the new file be sure to update your desktop application list by running `update-desktop-database ~/.local/share/applications/`
