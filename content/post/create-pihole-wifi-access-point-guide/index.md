+++
author = "unkwn1"
title = "Setting Up a PiHole Wifi Subnet"
date = "2023-03-26"
description = "A simple guide to create a PiHole wireless access point on a debian machine."
tags = [
    "networking",
    "pihole",
    "firewall",
    "adblocking",
]
categories = [
    "guides",
]
series = [
    "My LAN",
]
+++

I remember the good ol' days when I would use the default router wifi setup that comes with ISP internet packages.  Currently I have a raspberrypi that's connected to the aforementioned network via wifi bridging that connection the outside to an ethernet network.

The ethernet network which connects my proxmox LAN computers now has a debian based VM with a wifi USB passed through created a pihole wireless AP. Below is a general overview of how I configured the VM - this should work outside of a VM as well.

## Creating Your Wireless Access Point

Creating an access point is incredibly simple with this set up - simply install `hostapd` with your package manager. Normally you would have to include a DHCP server however this isn't necessary as the pihole software includes it.

### Setting a Static IP

`sudo nano /etc/dhcpd.conf`

```conf
interface wlan0
    static ip_address=192.168.4.1/24
    nohook wpa_supplicant
```

> always double check network interface names - don't assume yours will be the same!

`sudo systemctl daemon-reload`

`sudo service dhcpcd restart`

### Configuring hostapd

`sudo apt install hostapd`

`sudo nano /etc/hostapd/hostapd.conf`

```
interface=wlan0
driver=nl80211
ssid=0wa
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=abcdefgh
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

> change `ssid`, `wpa_passphrase` and, make sure the `interface` matches yours.

`sudo chmod 600 /etc/hostapd/hostapd.conf
`
`sudo nano /etc/default/hostapd`

`DAEMON_CONF="/etc/hostapd/hostapd.conf"`

```
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl start hostapd
```

### Enabling IP Forwarding

> double check your installation has `iptables` installed - my minimal debian install didn't :(

At this point your network should be visible however it wont have a connection to the internet. To fix this we're going to enable ip forwarding.

`sudo nano /etc/sysctl.conf`

Uncomment the line: `net.ipv4.ip_forward=1`

Next, we want to enable `masquerade` on the internet connected interface using iptables. Gentle reminder to take note of your interface as it may not be `eth0`.

`sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE`

Use the following command to save the iptables rule you set above to a file: `sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"`

**For raspbian use :**
Add the following: `iptables-restore < /etc/iptables.ipv4.nat` to `/etc/rc.local` to have the rule re-added on restart.

**For debian use :**
`apt install iptables-persistent`

## Installing Pihole

As this setup uses a VM the simplest route is to use the easy install script.

`wget -O basic-install.sh https://install.pi-hole.net`

`sudo bash basic-install.sh`

Once you're done the installation run the following commands:

`pihole admin -i all` and `pihole admin -i local`

### Enabled Pihole DHCP Server

`<YOUR PIHOLE IP>/admin/settings.php?tab=piholedhcp`

Finally, change the pihole web admin password using `pihole -a -p`.


## (IMPORTANT!) Improving Default Blocklists

Surprisingly the default blocklist doesn't do much. I found many ads still appearing. Compared to ublock origin and it's magic I was quite let down seeing only about 9% to a max of 11% of queries being blocked.

After some googling I found a tool to automate adding community curated blocklists making the pihole a bit better (cannot ever replace ublock origin tbh).

Ensure you have `pip` installed on your computer and then install `sudo pip3 install pihole5-list-tool`.

Then run `sudo pihole5-list-tool` and follow the onscreen prompts.


## After Thoughts / TODO

- [ ] look into changing the hw_mode, 2.4G speeds kind of suck
- [ ] compare ublock origin blocklists to pihole5-list-tool
