+++
author = "unkwn1"
title = "Fixing CI/CD Errors using Hugo via GitLab Pages "
date = "2023-01-01"
description = "Ran into a couple issues when posting my latest piece. A mix of hugo as go modules and pre-exisiting use of theme via submodule."
toc = true
tags = [
    "shell scripting",
    "gitlab",
    "git",
]
categories = [
    "Projects",
    "CI/CD",
    "DevOps",
]
+++


# No go Binary Found / SCSS Conversion Errors

I was attempting to push my latest blog changes which included a [new post](https://unkwn1.dev/post/newyearseve-2022-resolution/). Something so benign turned into a cuss-filled smh moment. 

I used GitLab pages to host me personal website https://unkwn1.dev/ and generally it's smooth. Until now any hiccups were caused solely by my own foolery. However today's dozen or so attempted commits to publish a single new post had me enter full reeee state.

The first error I received was laughably infuriating - keep in mind nothing since my last post was published has been changed. So I commit the changes which include the new markdown file, the CI pipeline begins to run and.... :sigh: apparently the CI image cannot find a binary named "go".... For real bro?????

So I play with some changes, notably I don't use the `:latest` tag and instead specify a Hugo version. Well that at least lead me to a *new* error. This time it was some `scss` error. Thankfully the error message at least hinted I should use the `hugo_extended` version.

## TLDR - Updated Hugo_Extended Pipeline

```yaml
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

# Set this if you intend to use Git submodules
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  HUGO_ENV: production

default:
  before_script:
    - apk add --update --no-cache git go
    - git submodule update --init --recursive
    - hugo mod init gitlab.com/pages/hugo
    - hugo mod get -u github.com/theNewDynamic/gohugo-theme-ananke

test:
  script:
    - hugo
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

pages:
  script:
    - hugo
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH


```
